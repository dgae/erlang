-module(wk1assign).
-export([perimeter/1,area/1,enclose/1,
        bitstail/1,bitsdirect/1,
        peritest1/0,peritest2/0,peritest3/0,peritest4/0,peritest5/0,
        areatest1/0,areatest2/0,areatest3/0,areatest4/0,areatest5/0,
        testtail1/0,testtail2/0,testtail3/0,
        enclosetest1/0,enclosetest2/0,enclosetest3/0,enclosetest4/0,
        testdirect1/0,testdirect2/0,testdirect3/0]).

% Calculating perimeter for different shapes

% Perimeter of a circle
perimeter({circle, {_X,_Y}, R}) ->
    2 * R * math:pi();

% Permieter of a rectangle
perimeter({rectangle, {_X,_Y}, H, W}) ->
    2 * (H + W) ;

% Perimeter of a square
perimeter({square, {_X,_Y}, H}) ->
    4 * H;

% Perimeter of triangle with given sides.
perimeter({triangles, {_X,_Y},  A, B, C}) ->
    A + B + C;

% Perimeter of triangle with given corner coordinates
perimeter({trianglec,  {X1,Y1}, {X2,Y2}, {X3,Y3}}) ->
    math:sqrt((X3 - X1) * (X3 - X1) + (Y3 - Y1) * (Y3 - Y1)) +
    math:sqrt((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2)) +
    math:sqrt((X3 - X2) * (X3 - X2) + (Y3 - Y2) * (Y3 - Y2)).

% Calculating the area for different shapes

% Area or a circle
area({circle, {_X,_Y}, R}) ->
    R * R * math:pi();

% Area of a rectangle
area({rectangle, {_X,_Y}, H, W}) ->
    H * W;

% Area of a square
area({square, {_X,_Y}, H}) ->
    H * H;

% Area of a triangle with given sides.
area({triangles, {0,0}, A, B, C}) -> % by given side length
    S = ( A + B + C ) / 2,
    math:sqrt(S * (S - A) * (S - B) * (S - C));

% Area of a triangle with given corner coordinates
area({trianglec, {X1,Y1}, {X2,Y2}, {X3,Y3} }) ->
    A = math:sqrt((X3 - X1) * (X3 - X1) + (Y3 - Y1) * (Y3 - Y1)),
    B = math:sqrt((X2 - X1) * (X2 - X1) + (Y2 - Y1) * (Y2 - Y1)),
    C = math:sqrt((X3 - X2) * (X3 - X2) + (Y3 - Y2) * (Y3 - Y2)),
    S = (A + B + C) / 2,
    math:sqrt(S * (S - A) * (S - B) * (S - C)).

% Enclosed shape by a rectangle

enclose({circle, {_X,_Y}, R}) ->
    {rectangle, {_X-R,_Y-R}, 2 * R, 2 * R};
    
enclose({rectangle, {_X,_Y}, H, W}) ->
    {rectangle, {_X,_Y}, H, W};

enclose({square, {_X,_Y}, H}) ->
    {rectangle, {_X,_Y}, H, H};

enclose({trianglec, {X1,Y1}, {X2,Y2}, {X3,Y3} }) ->
    % Calc min and max points for the rectangle
    XMIN = min(min(X1,X2),X3),
    YMIN = min(min(Y1,Y2),Y3),
    XMAX = max(max(X1,X2),X3),
    YMAX = max(max(Y1,Y2),Y3),

    {rectangle, {XMIN, YMIN}, (XMAX-XMIN), (YMAX-YMIN)}.



% bitstail(N) in recursive tail version
bitstail(N) when N > 0 ->
    % Call recursive funktion bitsum/3
    % bitsum(N,S)
    % N = Integer Number
    % S = Sum of Bits
    bitsum(N,0).

% Base Case - N or the remainder is 0
bitsum(0,S) ->
    S;
bitsum(N,S) when N > 0 ->
    % Call bitsum with new N and current S
    bitsum(N div 2, (S + N rem 2)).

% bitsdirect(N) in recursive direct version
% Bse Case N is 0
bitsdirect(0) ->
    0;
% Divide N by 2 if there is a remainder, the bit is set.
% Call function bitsdirect with the integer result of the integer division N/2
bitsdirect(N) when N > 0 ->
    N rem 2 + bitsdirect(N div 2).

% I think the tail version is better, since it carries the result to the next step
% and doesn't need to keep the result from earlier steps in Memory.
% Therefore it uses less memory and is also more time efficient.

% Perimeter tests
peritest1() ->
    wk1assign:perimeter({circle, {0, 0}, 1}) == 2*math:pi().
peritest2() ->
    wk1assign:perimeter({rectangle, {0, 0}, 3, 5}) == 16.
peritest3() ->
    wk1assign:perimeter({square, {0, 0}, 3}) == 12.
peritest4() ->
    wk1assign:perimeter({triangles, {0, 0}, 3, 4, 5}) == 12.
peritest5() ->
    wk1assign:perimeter({trianglec, {0, 0}, {3,0}, {3,4}}) == 12.

% Area Tests 
areatest1() ->
    wk1assign:area({circle, {0, 0}, 1}) == math:pi().
areatest2() ->
    wk1assign:area({rectangle, {0, 0}, 3, 8}) == 24.
areatest3() ->
    wk1assign:area({square, {0, 0}, 3}) == 9.
areatest4() ->
    wk1assign:area({triangles, {0, 0}, 3, 4, 5}) == 6.
areatest5() ->
    wk1assign:area({trianglec, {0, 0}, {3, 0}, {3, 4}}) == 6.

% Enclosre Tests
enclosetest1() ->
    wk1assign:enclose({circle, {0,0}, 5}) == {rectangle,{-5,-5},10,10}.
enclosetest2() ->
    wk1assign:enclose({rectangle, {0,0}, 4, 5}) == {rectangle,{0,0},4,5}.
enclosetest3() ->
    wk1assign:enclose({square, {0,0}, 7}) == {rectangle,{0,0}, 7, 7}.
enclosetest4() ->
    wk1assign:enclose({trianglec, {0,0}, {3,0}, {3,4}}) == {rectangle,{0,0},3,4}.


% Testcases tail recursive
testtail1() ->
    wk1assign:bitstail(1) == 1.
testtail2() ->
    wk1assign:bitstail(127) == 7.
testtail3() ->
    wk1assign:bitstail(128) == 1.

% Testcases for direct recursive
testdirect1() ->
    wk1assign:bitsdirect(1) == 1.
testdirect2() ->
    wk1assign:bitsdirect(127) == 7.
testdirect3() ->
    wk1assign:bitsdirect(128) == 1.
